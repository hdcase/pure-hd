# shellcheck disable=2034,2154

pure_async_git_aliases() {
    setopt localoptions noshwordsplit
    local -a gitalias pullalias parts
    local line

    # List all aliases and split on newline
    gitalias=("${(f@)$(command git config --get-regexp "^alias\.")}")
    for line in "${gitalias[@]}"; do
        parts=("${(@)line}")          # split line on spaces
        aliasname=${parts[1]#alias.}  # grab the name (alias.[name])
        shift parts                   # remove aliasname

        # check alias for pull or fetch (must be exact match)
        if [[ ${parts[*]} =~ ^(.*\ )?(pull|fetch)(\ .*)?$ ]]; then
            pullalias+=("$aliasname")
        fi
    done

    print -- "${(j:|:)pullalias[@]}"  # join on pipe (for use in regex).
}

pure_async_vcs_info() {
    setopt localoptions noshwordsplit
    local -A info

    # Configure vcs_info inside async task, this frees up vcs_info
    # to be used or configured as the user pleases
    zstyle ':vcs_info:*' enable git
    zstyle ':vcs_info:*' use-simple true

    # Only export two msg variables from vcs_info
    zstyle ':vcs_info:*' max-exports 2

    # Export branch (%b) and git toplevel (%R)
    zstyle ':vcs_info:git*' formats '%b' '%R'
    zstyle ':vcs_info:git*' actionformats '%b|%a' '%R'

    vcs_info
    info[pwd]=$PWD
    info[top]=$vcs_info_msg_1_
    info[branch]=$vcs_info_msg_0_
    print -r - "${(@kvq)info[@]}"
}

pure_async_git_dirty() {
    setopt localoptions noshwordsplit

    if [[ $1 || $pure_git_arrows ]]; then
        return 1
    fi
}

pure_async_git_fetch() {
    setopt localoptions noshwordsplit localtraps monitor
    # Default return code, indicates Git fetch failure
    local fail_code=99
    # set GIT_TERMINAL_PROMPT=0 to disable auth prompting for git fetch
    export GIT_TERMINAL_PROMPT=0
    # set ssh BatchMode to disable all interactive ssh password prompting
    export GIT_SSH_COMMAND="${GIT_SSH_COMMAND:-ssh} -o BatchMode=yes"

    # Make sure local HUP trap is unset to allow for
    # signal propagation when the async worker is flushed
    trap - HUP

    trap '
        # Unset trap to prevent infinite loop
        trap - CHLD
        if [[ $jobstates = suspended* ]]; then
            # Set fail code to password prompt and kill the fetch.
            fail_code=98
            kill %%
        fi
    ' CHLD

    command git -c gc.auto=0 fetch >/dev/null &
    wait $! || return "$fail_code"

    unsetopt monitor

    # Check arrow status after a successful git fetch
    pure_async_git_arrows
}

pure_async_git_arrows() {
    setopt localoptions noshwordsplit

    command git rev-list --left-right --count 'HEAD'...@'{upstream}'
}

pure_check_git_arrows() {
    setopt localoptions noshwordsplit
    local arrows
    local -i left="$1" right="$2"

    (( right > 0 )) && arrows+="%F{magenta}⇣${right}%f "
    (( left > 0 )) && arrows+="%F{cyan}⇡${left}%f"

    [[ $arrows ]] || return
    typeset -g REPLY="${arrows% }"
}

pure_async_git_untracked() {
    setopt localoptions noshwordsplit
    local pure_git_untracked
    pure_git_untracked=${(w)#${(f@)1}%%[^?]*}

    if (( pure_git_untracked == 0 )); then
        return 1
    else
        printf '%s' "$pure_git_untracked"
    fi
}

pure_async_git_modified() {
    setopt localoptions noshwordsplit
    local pure_git_modified
    pure_git_modified=${(w)#${(f@)1}%%[^ M]*}

    (( pure_git_modified == 0 )) || printf '%s' "$pure_git_modified"
}

pure_async_git_staged() {
    setopt localoptions noshwordsplit
    local pure_git_staged
    pure_git_staged=${(w)#${(f@)1}%%[^(A|M)]*}

    (( pure_git_staged == 0 )) || printf '%s' "$pure_git_staged"
}

