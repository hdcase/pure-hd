# shellcheck disable=2034,2154,2190

pure_async_tasks() {
    setopt localoptions noshwordsplit
    typeset -ig pure_async_init
    typeset -gA pure_vcs_info
    local -H MATCH MBEGIN MEND

    # Initialize async worker
    if (( ! pure_async_init )); then
        async_start_worker 'pure' -u -n
        async_register_callback 'pure' pure_async_callback
        pure_async_init=1
    fi

    # Update the current working directory of the async worker.
    async_worker_eval 'pure' builtin cd -q "$PWD"

    if [[ $PWD != ${pure_vcs_info[pwd]}* ]]; then
        # stop any running async jobs
        async_flush_jobs 'pure'

        # Reset git preprompt variables, switching working tree
        unset pure_git_dirty pure_git_dirty_timestamp \
                pure_git_arrows pure_git_fetch pure_git_untracked \
                pure_git_modified pure_git_staged
        pure_vcs_info[branch]=
        pure_vcs_info[top]=
    fi

    unset MATCH MBEGIN MEND

    async_job 'pure' pure_async_vcs_info

    # Only perform tasks inside git working tree
    [[ ${pure_vcs_info[top]} ]] || return

    pure_async_refresh
}

pure_async_refresh() {
    setopt localoptions noshwordsplit
    typeset -g pure_git_status
    local -a pure_git_types=('_untracked' '_modified' '_staged')
    local _pure_git_type
    typeset -g pure_git_fetch
    typeset -i time_since_dirty_check

    # Cannot be run asynchronously due to requirement for other jobs
    pure_git_status=$(command git status --porcelain -unormal 2>/dev/null)

    # We set the pattern here to avoid redoing the pattern check until the
    # working tree has changed - pull and fetch are always valid patterns
    if [[ -z $pure_git_fetch ]]; then
        pure_git_fetch='pull|fetch'
        async_job 'pure' pure_async_git_aliases
    fi

    async_job 'pure' pure_async_git_arrows

    for _pure_git_type in "${pure_git_types[@]}"; do
        async_job 'pure' pure_async_git"$_pure_git_type" "$pure_git_status"
    done

    # Do not perform git fetch if in home folder.
    if [[ ${pure_vcs_info[top]} != "$HOME" ]]; then
        async_job 'pure' pure_async_git_fetch
    fi

    # If dirty checking is sufficiently fast, tell worker
    # to check it again, otherwise wait for timeout
    time_since_dirty_check=$(( EPOCHSECONDS - ${pure_git_dirty_timestamp:-0} ))

    # Check if there is anything to pull
    if (( time_since_dirty_check > 1800 )); then
        unset pure_git_dirty_timestamp
        async_job 'pure' pure_async_git_dirty "$pure_git_status"
    fi
}

pure_async_callback() {
    setopt localoptions noshwordsplit
    local job="$1" code="$2" output="$3" exec_time="$4" next_pending="$6" \
            do_render=0

    case "$job" in
        \[async])
            # code is 1 for corrupted worker output and 2 for dead worker
            if (( code == 2 )); then
                # our worker died unexpectedly
                typeset -g pure_async_init=0
            fi
            ;;
        pure_async_vcs_info)
            local -A info
            typeset -gA pure_vcs_info
            local -H MATCH MBEGIN MEND

            # parse output (z) and unquote as array (Q@)
            info=("${(Q@)${(z)output}}")

            # The path has changed since the check started, abort.
            if [[ ${info[pwd]} != "$PWD" ]]; then
                return
            fi

            # check if git toplevel has changed
            if [[ ${info[top]} = "${pure_vcs_info[top]}" ]]; then
                # If stored pwd is part of $PWD, $PWD is shorter
                # and likelier to be toplevel, so we update pwd
                if [[ ${pure_vcs_info[pwd]} = ${PWD}* ]]; then
                    pure_vcs_info[pwd]=$PWD
                fi
            else
                # Store $PWD to detect if we (maybe) left the git path
                pure_vcs_info[pwd]=$PWD
            fi

            unset MATCH MBEGIN MEND

            # Update has a git toplevel set which means we just
            # entered a new git directory, run the async refresh tasks
            [[ ${info[top]} ]] &&
                [[ -z ${pure_vcs_info[top]} ]] &&
                pure_async_refresh

            # Always update branch and toplevel
            pure_vcs_info[branch]=${info[branch]}
            pure_vcs_info[top]=${info[top]}

            do_render=1
            ;;
        pure_async_git_aliases)
            [[ ! $output ]] || pure_git_fetch+="|$output" ;;
        pure_async_git_dirty)
            local prev_dirty="$pure_git_dirty"

            if (( code == 0 )); then
                #unset pure_git_dirty
                typeset -g pure_git_dirty='%F{green}✔%f'
            else
                typeset -g pure_git_dirty='%F{red}✘%f'
            fi

            [[ $prev_dirty != "$pure_git_dirty" ]] && do_render=1

            # When pure_git_dirty_timestamp is set, the git info
            # is displayed in a different color.
            # To distinguish between a "fresh" and a "cached" result,
            # the preprompt is rendered before setting this variable.
            # Thus, only upon next rendering of the preprompt will the
            # result appear in a different color.
            (( exec_time > 5 )) && pure_git_dirty_timestamp=$EPOCHSECONDS
            ;;
        pure_async_git_fetch|pure_async_git_arrows)
            # pure_async_git_fetch executes pure_async_git_arrows
            # after a successful fetch
            case "$code" in
                0)
                    local REPLY
                    pure_check_git_arrows "${(ps:\t:)output}"
                    if [[ $pure_git_arrows != "$REPLY" ]]; then
                        typeset -g pure_git_arrows="$REPLY"
                        do_render=1
                    fi
                    ;;
                99|98)
                    # Git fetch failed
                    ;;
                *)
                    # Non-zero exit status from pure_async_git_arrows,
                    # indicating that there is no upstream configured.
                    if [[ $pure_git_arrows ]]; then
                        unset pure_git_arrows
                        do_render=1
                    fi
                    ;;
            esac
            ;;
        pure_async_git_untracked)
            #if (( output > 0 )); then
            if (( code == 0 )); then
                typeset -g pure_git_untracked="$output"
                do_render=1
            else
                unset pure_git_untracked
            fi
            ;;
        pure_async_git_modified)
            if (( output > 0 )); then
                typeset -g pure_git_modified="$output"
                do_render=1
            else
                unset pure_git_modified
            fi
            ;;
        pure_async_git_staged)
            if (( output > 0 )); then
                typeset -g pure_git_staged="$output"
                do_render=1
            else
                unset pure_git_staged
            fi
            ;;
    esac

    if (( next_pending )); then
        (( do_render )) && typeset -g pure_async_render_requested=1
        return
    fi

    (( ${pure_async_render_requested:-${do_render}} == 1 )) &&
        pure_preprompt_render
    unset pure_async_render_requested
}

