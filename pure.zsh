# Pure-HD
# Original
# by Sindre Sorhus
# Port
# by H. D. Case
# https://gitlab.com/hdcase/pure
# MIT License

# For my own and others sanity
# git:
# %b => current branch
# %a => current action (rebase/merge)
# prompt:
# %F => color dict
# %f => reset color
# %~ => current path
# %* => time
# %n => username
# %m => shortname host
# %(?..) => prompt conditional - %(condition.true.false)
# terminal codes:
# \e7   => save cursor position
# \e[2A => move cursor 2 lines up
# \e[1G => go to position 1 in terminal
# \e8   => restore cursor position
# \e[K  => clears everything after the cursor on the current line
# \e[2K => clear everything on the current line
#
# shellcheck disable=1090,2034,2154

PURE_HOME=${(%):-%x}
source "${PURE_HOME%/*}/pure-pre.zsh"
source "${PURE_HOME%/*}/pure-render.zsh"
source "${PURE_HOME%/*}/pure-time.zsh"
source "${PURE_HOME%/*}/pure-git.zsh"
source "${PURE_HOME%/*}/pure-async.zsh"
source "${PURE_HOME%/*}/pure-debug.zsh"

pure_main() {
    # Prevent percentage showing up if output doesn't end with a newline.
    export PROMPT_EOL_MARK=
    typeset -g pure_cmd_timestamp="$EPOCHSECONDS"

    # Borrowed from promptinit, sets the prompt options
    # in case pure was not initialized via promptinit
    setopt 'noprompt'{bang,cr,percent,subst} 'prompt'{subst,percent}

    # This variable needs to be set, usually set by promptinit
    if [[ -z $prompt_newline ]]; then
        typeset -g prompt_newline=$'\n%{\r%}'
    fi

    zmodload 'zsh/datetime' 'zsh/zle' 'zsh/parameter'
    autoload -Uz 'add-zsh-hook' 'vcs_info' 'async' && async

    # The add-zle-hook-widget function is not guaranteed
    # to be available, it was added in Zsh 5.3.
    autoload -Uz +X 'add-zle-hook-widget' 2>/dev/null

    add-zsh-hook precmd 'pure_precmd'
    add-zsh-hook preexec 'pure_preexec'

    pure_state_setup

    zle -N 'pure_update_vim_prompt_widget'
    zle -N 'pure_reset_vim_prompt_widget'

    if (( ${+functions[add-zle-hook-widget]} )); then
        add-zle-hook-widget zle-line-finish 'pure_reset_vim_prompt_widget'
        add-zle-hook-widget zle-keymap-select 'pure_update_vim_prompt_widget'
    fi

    # If a virtualenv is activated, display it in grey
    PROMPT='%(12V.%F{242}%12v%f .)'

    # Prompt turns red if the previous command didn't exit with 0
    PROMPT+=$'%(?.%F{green}.%F{red})${pure_state[prompt]}%f '

    pure_debug_prompt
}

pure_main "$@"

