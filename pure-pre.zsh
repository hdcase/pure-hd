# shellcheck disable=2034,2154,2190

pure_precmd() {
    # Check exec time and store it in a variable
    pure_check_cmd_exec_time
    unset pure_cmd_timestamp

    # Shows the full path in the title
    pure_set_title 'expand-prompt' '%~'

    # preform async git dirty check and fetch
    pure_async_tasks

    # Check if we should display the virtual env, we use a sufficiently high
    # index of psvar (12) here to avoid collisions with user defined entries
    psvar[12]=

    # Check if a conda environment is active and display it's name
    if [[ -n $CONDA_DEFAULT_ENV ]]; then
        psvar[12]="${CONDA_DEFAULT_ENV//[$'\t\r\n']}"
    fi

    # When VIRTUAL_ENV_DISABLE_PROMPT is empty, it was unset by the user and
    # Pure should take back control
    if [[ $VIRTUAL_ENV && $VIRTUAL_ENV_DISABLE_PROMPT = 12 ]]; then
        psvar[12]=${VIRTUAL_ENV:t}
        #export VIRTUAL_ENV_DISABLE_PROMPT=12
    fi

    # Make sure VIM prompt is reset.
    pure_reset_prompt_symbol

    # Print the preprompt
    pure_preprompt_render 'precmd'

    if [[ -n $ZSH_THEME ]]; then
        printf '%s%s%s\n' 'WARNING: Oh My Zsh themes are enabled (ZSH_THEME=' \
                "'${ZSH_THEME}'" '). Pure might not be working correctly.'
        printf '%s %s\n' 'For more information, see:' \
                'https://github.com/sindresorhus/pure#oh-my-zsh'
        unset ZSH_THEME  # Only show this warning once.
    fi
}

pure_preexec() {
    if [[ $pure_git_fetch ]]; then
        # Detect when git is performing pull/fetch (including git aliases)
        local -H MATCH MBEGIN MEND match mbegin mend
        if [[ $2 =~ (git|hub)\ (.*\ )?($pure_git_fetch)(\ .*)?$ ]]; then
            # We must flush the async jobs to cancel our git fetch in order
            # to avoid conflicts with the user issued pull / fetch.
            async_flush_jobs 'pure'
        fi
    fi

    typeset -g pure_cmd_timestamp="$EPOCHSECONDS"

    # Shows the current dir and executed command
    # in the titlewhile a process is active
    pure_set_title 'ignore-escape' "${PWD}:t: $2"

    # Disallow python virtualenv from updating the prompt, set it to 12 if
    # untouched by the user to indicate that Pure modified it. Here we use
    # magic number 12, same as in psvar
    export VIRTUAL_ENV_DISABLE_PROMPT=12
}

pure_reset_prompt_symbol() {
    pure_state[prompt]='❯'
}

pure_update_vim_prompt_widget() {
    setopt localoptions noshwordsplit

    pure_state[prompt]=${${KEYMAP/vicmd/❮}/(main|viins)/❯}
    zle && zle .reset-prompt
}

pure_reset_vim_prompt_widget() {
    setopt localoptions noshwordsplit

    pure_reset_prompt_symbol
    zle && zle .reset-prompt
}

pure_state_setup() {
    setopt localoptions noshwordsplit
    local ssh_connection="${SSH_CONNECTION:-${PURE_SSH_CONNECTION}}" username
    typeset -gA pure_state

    # Check SSH_CONNECTION and the current state
    if [[ -z $ssh_connection ]] && [[ ${commands[who]} ]]; then
        # When changing user on a remote system, the $SSH_CONNECTION
        # environment variable can be lost, attempt detection via who.
        local who_out

        if ! who_out=$(who -m 2>/dev/null); then
            # Who am I not supported, fallback to plain who.
            who_out=$(who 2>/dev/null | grep "${TTY#/dev/}")
        fi

        local reIP6='(([0-9a-fA-F]+:)|:){2,}[0-9a-fA-F]+'
        local reIP4='([0-9]{1,3}\.){3}[0-9]+'
        # Here we assume two non-consecutive periods represents a
        # hostname. This matches foo.bar.baz, but not foo.bar.
        local reHostname='([.][^. ]+){2}'

        # Usually the remote address is surrounded by parenthesis, but
        # not on all systems (e.g. busybox).
        local -H MATCH MBEGIN MEND

        if [[ $who_out =~ \\(?(${reIP4}|${reIP6}|${reHostname})\\)?\$ ]]; then
            ssh_connection=$MATCH

            # Export variable to allow detection propagation inside
            # shells spawned by this one (e.g. tmux does not always
            # inherit the same tty, which breaks detection).
            export PURE_SSH_CONNECTION="$ssh_connection"
        fi

        unset MATCH MBEGIN MEND
    fi

    # Show username@host if logged in through SSH
    [[ $ssh_connection ]] && username='%F{242}%n@%m%f'

    # Show username@host if root, with username in white
    (( UID == 0 )) && username='%F{white}%n%f%F{242}@%m%f'

    pure_state=('username' "$username" 'prompt' '❯')
}

