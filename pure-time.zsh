# Turns seconds into human readable time
# 165392 => 1d 21h 56m 32s
# https://github.com/sindresorhus/pretty-time-zsh
#
# shellcheck disable=2034

pure_human_time() {
    local human total_seconds="$1"
    local days="$(( total_seconds / 60 / 60 / 24 ))"
    local hours="$(( total_seconds / 60 / 60 % 24 ))"
    local minutes="$(( total_seconds / 60 % 60 ))"
    local seconds="$(( total_seconds % 60 ))"
    (( days > 0 )) && human+="${days}d "
    (( hours > 0 )) && human+="${hours}h "
    (( minutes > 0 )) && human+="${minutes}m "
    human+="${seconds}s"

    # store human readable time in variable as specified by caller
    typeset -g pure_cmd_exec_time="$human"
}

# Stores (into pure_cmd_exec_time) the exec time
# time of the last command if set threshold was exceeded
pure_check_cmd_exec_time() {
    integer elapsed
    unset pure_cmd_exec_time
    (( elapsed = EPOCHSECONDS - ${pure_cmd_timestamp:-$EPOCHSECONDS} ))
    (( elapsed > 5 )) && pure_human_time $elapsed
}

