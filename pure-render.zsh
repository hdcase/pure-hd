# shellcheck disable=2034,2154

pure_set_title() {
    setopt localoptions noshwordsplit
    local hostname=
    local -a opts

    # Don't set title over serial console.
    case "$TTY" in
        /dev/ttyS[0-9]*)
            return ;;
    esac

    # Show hostname if connected via ssh.
    if [[ -n ${pure_state[username]} ]]; then
        # Expand in-place in case ignore-escape is used.
        hostname="${(%):-(%m) }"
    fi

    case "$1" in
        expand-prompt)
            opts=(-P) ;;
        ignore-escape)
            opts=(-r) ;;
    esac

    # Set title atomically in one print statement so that it works
    # when XTRACE is enabled.
    print -n "${opts[@]}" $'\e]0;'"${hostname}$2"$'\a'
}

pure_preprompt_render() {
    setopt localoptions noshwordsplit

    # Set color for git branch/dirty status, change
    # color if dirty checking has been delayed
    local git_color=242 cleaned_ps1="$PROMPT" expanded_prompt

    # Initialize the preprompt array
    local -a preprompt_parts
    typeset -gA pure_vcs_info
    local -H MATCH MBEGIN MEND
    local -ah ps1

    [[ ${pure_git_dirty_timestamp:+x} ]] && git_color=red

    # Set the path
    preprompt_parts+=('%F{blue}%~%f')

    # Add git branch and dirty status info
    if [[ ${pure_vcs_info[branch]} ]]; then
        preprompt_parts+=("%F{${git_color}}"$'${pure_vcs_info[branch]}%f')
        preprompt_parts+=($'%F{yellow}${pure_git_dirty}%f')
    fi

    # Git pull/push arrows
    [[ ! $pure_git_arrows ]] || preprompt_parts+=($'$pure_git_arrows')

    # Git untracked files
    [[ ! $pure_git_untracked ]] ||
        preprompt_parts+=($'%F{yellow}${pure_git_untracked}%f')

    # Git modified files
    [[ ! $pure_git_modified ]] ||
        preprompt_parts+=($'%F{183}✚${pure_git_modified}%f')

    # Git staged files
    [[ ! $pure_git_staged ]] ||
        preprompt_parts+=($'%F{purple}●${pure_git_staged}%f')

    # Username and machine, if applicable
    [[ ${pure_state[username]} ]] &&
        preprompt_parts+=($'${pure_state[username]}')

    # Execution time
    [[ $pure_cmd_exec_time ]] &&
        preprompt_parts+=($'%F{yellow}${pure_cmd_exec_time}%f')

    # Remove everything from the prompt until the newline
    # This removes the preprompt and only the original PROMPT remains
    if [[ $PROMPT = *"$prompt_newline"* ]]; then
        cleaned_ps1=${PROMPT##*${prompt_newline}}
    fi

    unset MATCH MBEGIN MEND

    # Construct the new prompt with a clean preprompt
    ps1=(
        "${(j. .)preprompt_parts}"  # Join parts, space separated.
        "$prompt_newline"           # Separate preprompt and prompt.
        "$cleaned_ps1"
    )

    PROMPT=${(j..)ps1}

    # Expand the prompt for future comparision
    expanded_prompt=${(S%%)PROMPT}

    if [[ $1 == precmd ]]; then
        # Initial newline, for spaciousness
        print
    elif [[ $pure_last_prompt != "$expanded_prompt" ]]; then
        # Redraw the prompt
        zle && zle .reset-prompt
    fi

    typeset -g pure_last_prompt="$expanded_prompt"
}

